package com.launchwindows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import com.launchwindows.resources.LaunchWindowsResource;

@SpringBootApplication
@Import({ LaunchWindowsResource.class })
public class LaunchWindowsAppApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LaunchWindowsAppApplication.class, args);
	}

}
