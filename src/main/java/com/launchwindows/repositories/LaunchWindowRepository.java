package com.launchwindows.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.launchwindows.entities.LaunchWindowEntity;

@Repository
public interface LaunchWindowRepository extends JpaRepository<LaunchWindowEntity, Integer> {
	LaunchWindowEntity findByLocationId(Integer id);
}
