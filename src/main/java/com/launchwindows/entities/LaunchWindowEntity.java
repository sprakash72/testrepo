package com.launchwindows.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "launchwindow")
public class LaunchWindowEntity {
	@Id
	@Column(name = "citycode")
	private Integer locationId;
	@Column(name = "querytime")
	private Date queryDateTime;
	@Column(name = "windows")
	private String results;

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Date getQueryDateTime() {
		return queryDateTime;
	}

	public void setQueryDateTime(Date queryDateTime) {
		this.queryDateTime = queryDateTime;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

}
