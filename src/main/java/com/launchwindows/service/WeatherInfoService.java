package com.launchwindows.service;

import com.launchwindows.model.LaunchWindows;

public interface WeatherInfoService {
	LaunchWindows getWeatherInfo(String location);

	LaunchWindows getWeatherInfo();
}
