package com.launchwindows.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.boot.json.JsonParseException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.launchwindows.entities.LaunchWindowEntity;
import com.launchwindows.exception.LocationNotfoundException;
import com.launchwindows.model.LaunchWindow;
import com.launchwindows.model.LaunchWindows;
import com.launchwindows.model.weatherinfo.Constants;
import com.launchwindows.model.weatherinfo.WeatherInfo;
import com.launchwindows.model.weatherinfo.WeatherParameters;
import com.launchwindows.repositories.LaunchWindowRepository;
import com.launchwindows.service.WeatherInfoService;

@Service
public class WeatherInfoServiceImpl implements WeatherInfoService {

	@Resource
	private LaunchWindowRepository launchWindowRepository;

	private RestTemplate restTemplate = new RestTemplate();

	public LaunchWindows getWeatherInfo(String city) {

		String locationId = Constants.citytocodemap.get(city);
		if (locationId == null)
			throw new LocationNotfoundException();

		Integer id = Integer.parseInt(locationId);
		Optional<LaunchWindowEntity> entity = launchWindowRepository.findById(id);

		if (IsInfoAvailableInCache(entity) == true) {
			LaunchWindows launchWindows = null;
			LaunchWindowEntity lwobj = null;
			System.out.println("Getting Values from Database..");
			try {
				lwobj = entity.get();
				launchWindows = mapFromJson(lwobj.getResults(), LaunchWindows.class);
			} catch (JsonParseException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return launchWindows;
		}

		Date date = new Date();
		WeatherInfo locResponse = this.restTemplate
				.getForObject("https://api.openweathermap.org/data/2.5/forecast?id={locationId}&units=metric&appid="
						+ Constants.API_KEY, WeatherInfo.class, locationId);

		List<LaunchWindow> launchWindowlist = new ArrayList<>();
		launchWindowlist = ProcessWindowResults(launchWindowlist, locResponse, city);

		LaunchWindows launchWindows = CollectAndSort(launchWindowlist, 5);
		String jsonStr = "";
		try {
			jsonStr = mapToJson(launchWindows);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LaunchWindowEntity lwobj = new LaunchWindowEntity();
		lwobj.setLocationId(id);
		lwobj.setQueryDateTime(date);
		lwobj.setResults(jsonStr);
		launchWindowRepository.save(lwobj);

		return launchWindows;

	}

	@Override
	public LaunchWindows getWeatherInfo() {

		Optional<LaunchWindowEntity> entity = launchWindowRepository.findById(Constants.all);
		if (IsInfoAvailableInCache(entity) == true) {
			LaunchWindows launchWindows = null;
			LaunchWindowEntity lwobj = null;
			System.out.println("Getting Values from Database..");
			try {
				lwobj = entity.get();
				launchWindows = mapFromJson(lwobj.getResults(), LaunchWindows.class);
			} catch (JsonParseException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return launchWindows;
		}

		List<LaunchWindow> launchWindowlist = new ArrayList<>();
		Date date = new Date();

		for (Map.Entry<String, String> entry : Constants.citytocodemap.entrySet()) {
			String city = entry.getKey();
			String locationId = entry.getValue();

			WeatherInfo locResponse = this.restTemplate
					.getForObject("https://api.openweathermap.org/data/2.5/forecast?id={locationId}&units=metric&appid="
							+ Constants.API_KEY, WeatherInfo.class, locationId);

			launchWindowlist = ProcessWindowResults(launchWindowlist, locResponse, city);

		}

		LaunchWindows launchWindows = CollectAndSort(launchWindowlist, 15);
		String jsonStr = "";
		try {
			jsonStr = mapToJson(launchWindows);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LaunchWindowEntity lwobj = new LaunchWindowEntity();
		lwobj.setLocationId(Constants.all);
		lwobj.setQueryDateTime(date);
		lwobj.setResults(jsonStr);
		launchWindowRepository.save(lwobj);

		return launchWindows;
	}

	List<LaunchWindow> ProcessWindowResults(List<LaunchWindow> launchWindowlist, WeatherInfo locResponse, String city) {
		List<WeatherParameters> list = locResponse.getList();
		for (int i = 0; i < list.size(); ++i) {
			WeatherParameters params = list.get(i);
			Float temp = params.getMain().getTemp();
			Float windDir = params.getWind().getDeg();
			Float windSpeed = params.getWind().getSpeed();
			int clouds = params.getClouds().getAll();
			String datetime = params.getDt_txt();

			if (IsWindowValid(city, clouds, windSpeed) == false)
				continue;

			int launchWindowScore = GetLaunchScore(temp, windSpeed, windDir);

			LaunchWindow launchWindow = new LaunchWindow();
			launchWindow.setLocation(city);
			launchWindow.setDatetime(datetime);
			launchWindow.setScore(launchWindowScore);

			launchWindowlist.add(launchWindow);

//        	System.out.println(datetime + " " + temp + " " + windDir + " "+ windSpeed + " " + clouds);
		}

		return launchWindowlist;

	}

	boolean IsWindowValid(String city, int clouds, Float windSpeed) {
		if (Constants.cloudinessMap.containsKey(city) && clouds > Constants.cloudinessMap.get(city))
			return false;

		if (Constants.windspeedMap.containsKey(city) && clouds > Constants.windspeedMap.get(city))
			return false;

		return true;

	}

	int GetLaunchScore(Float temp, Float windSpeed, Float windDir) {
		return (int) (Math.abs(20 - temp) + windSpeed + Math.abs(220 - windDir) * 0.1);
	}

	LaunchWindows CollectAndSort(List<LaunchWindow> launchWindowlist, int maxsize) {
		// Sort WindowList on Score
		Collections.sort(launchWindowlist);
		LaunchWindows launchWindows = new LaunchWindows();

		// Create a sublist of at most top 5 results and add to window list
		launchWindows.setLaunchWindows(launchWindowlist.subList(0, Math.min(launchWindowlist.size(), maxsize)));

		return launchWindows;
	}

	boolean IsInfoAvailableInCache(Optional<LaunchWindowEntity> entity) {
//		Optional<LaunchWindowEntity> entity = launchWindowRepository.findById(id);
		if (entity.isPresent()) {
			System.out.println("Trying to fetch from Database..");
			Date date = new Date();
			LaunchWindowEntity lwobj = entity.get();
			Date dateStored = lwobj.getQueryDateTime();
			long diffInMillies = date.getTime() - dateStored.getTime();
			long timeDiffInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);

			System.out.println("Current Time: " + date.toString() + " Last Query Time: " + dateStored.toString()
					+ " Difference (minutes): " + timeDiffInMinutes);

			if (timeDiffInMinutes < Constants.threshold)
				return true;

		}
		return false;

	}

	/// Map java object to JSON
	protected String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}

	// Maps JSON to a java class T
	protected <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, clazz);
	}

}
