This is a spring boot project that will run as a server on local host, providing rest endpoints. To run the project do following steps
1. Install eGit plugin in eclipse for integration with bitbucket within eclipse. 
Go to Help->Install New Software. Click on Add button, and in dialog that opens paste location http://download.eclipse.org/egit/updates
Select options GitIntegration for eclipse & Java impl for Git. Click Finish
 
2.Import project in eclipse 
-File->import->Git->Projects from Git
-Go to Next screen & select 'Clone Uri"
-In next screen, provide following bitbucket repo path in URI https://bitbucket.org/sprakash72/testrepo. This is a public repository
-In next screen choose a destination local directory and click Next 
-Select import as a General Project & on next screen provide project name "LaunchWindowsApp" & click finish

2. Right click on project name, click configure & select "convert to maven project"
3. Select project name and Run as Java Application server is running on localhost:3000
4. Once app is running, open postman & enter below URLs
http://localhost:3000/launchwindows?location=Hobart  -> This will return 5 launch windows with lowest scores from Hobart
{
    "launchWindows": [
        
	{
            
	"location": "Hobart",
            
	"datetime": "2019-03-11 03:00:00",
            
	"score": 12
        
	},
        
	{
            
	"location": "Hobart",
            
	"datetime": "2019-03-10 12:00:00",
            
	"score": 13
        
	},
        
	{
            
	"location": "Hobart",
            
	"datetime": "2019-03-11 00:00:00",
            
	"score": 13
        
	},
        
	{
            
	"location": "Hobart",
            
	"datetime": "2019-03-13 00:00:00",
            
	"score": 13
        
	},
        
	{
            
	"location": "Hobart",
            
	"datetime": "2019-03-10 18:00:00",
            
	"score": 15
        
	}
    ]

}
http://localhost:3000/launchwindows -> This will return 15 launch windows with lowest scores from Melbourne, Hobart, Perth, Darwin
http://localhost:3000/launchwindows?location=Sydney -> error message, as it not a tracked location 


#Steps for deployment on AWS
1. Added or modified following files for serverless deployment
-sam.yaml
-StreamLambdaHandler.java
-config/Config.java
-LaunchWindowsApp.java: Modified to add Lambda profile
-pom.xml: modified to exclude embedded Tomcat from output JAR as serverless JAVA container library gets used on AWS and build code as Shaded jar instead of Spring Boot Jar 

2. mvn clean package
4. Configure AWS CLI, set access key & secret for AWS User
-aws configure
3. Create a unique S3 bucket using AWS CLI
-aws s3 mb s3://spring-boot-lambda-0001
4. Copy jar file to S3 bucket and update the information into output-sam.yaml
-aws cloudformation package --template-file sam.yaml --output-template-file target/output-sam.yaml --s3-bucket spring-boot-lambda-0001
5. Deploy Cloudformation stack from SAM template. Ensure AWS user has relevant permissions to access cloudformation via IAM policies
-aws cloudformation deploy --template-file target/output-sam.yaml --stack-name spring-boot-launchwindows-lambda --capabilities CAPABILITY_IAM
6. Describe stact to get URL form "OutputValue". This URL can be used to test API from rest client
-aws cloudformation describe-stacks --stack-name spring-boot-launchwindows-lambda
 "Outputs": [
                {
                    "Description": "URL for application",
                    "ExportName": "LambdaWindowsApi",
                    "OutputKey": "LambdaSpringBootApi",
                    "OutputValue": "https://fw5w75cc93.execute-api.ap-southeast-2.amazonaws.com/Stage/launchwindows"
                }
            ],







 

